# -*- coding: utf-8 -*-
#
# Copyright 2019  Ternaris.
# SPDX-License-Identifier: Apache-2.0

from dataclasses import dataclass, field
from typing import Any, AsyncGenerator, Dict, List, Union
from urllib.parse import quote_plus

import pendulum

from . import gitlab
from .job import Job


GET = gitlab.GET


class Pipeline(gitlab.Resource):
    @classmethod
    async def fetch_by_id(
            cls,
            api: gitlab.Api,
            id_or_path: Union[int, str],
            pipeline_id: int,
    ) -> 'Pipeline':
        id_or_path = quote_plus(id_or_path) if isinstance(id_or_path, str) else id_or_path
        item = cls(api, {'id': pipeline_id, 'project_id': id_or_path})
        await item.refetch_info()
        return item

    @classmethod
    async def search(
            cls,
            api: gitlab.Api,
            id_or_path: Union[int, str],
            params: Dict = None,
    ) -> AsyncGenerator['Pipeline', None]:
        id_or_path = quote_plus(id_or_path) if isinstance(id_or_path, str) else id_or_path
        request = GET(f'/projects/{id_or_path}/pipelines', params)
        async for info in api.collect_all_pages_(request):
            # TODO: get numeric id as it is not included in info
            info['project_id'] = id_or_path
            yield cls(api, info)

    @property
    def project_id(self):
        return self.info['project_id']

    @property
    def ref(self):
        return self.info['ref']

    @property
    def sha(self):
        return self.info['sha']

    @property
    def status(self):
        return self.info['status']

    @property
    def web_url(self):
        return self.info['web_url']

    @property
    def created_at(self) -> pendulum.DateTime:
        created_at = self.info['created_at']
        return pendulum.parse(created_at) if created_at else None  # type: ignore[return-value]

    @property
    def started_at(self) -> pendulum.DateTime:
        started_at = self.info['started_at']
        return pendulum.parse(started_at) if started_at else None  # type: ignore[return-value]

    @property
    def finished_at(self) -> pendulum.DateTime:
        finished_at = self.info['finished_at']
        return pendulum.parse(finished_at) if finished_at else None  # type: ignore[return-value]

    @property
    def delay(self):
        if not self.started_at:
            return None
        return self.started_at - self.created_at

    @property
    def delay_seconds(self):
        delay = self.delay
        return round(delay.total_seconds()) if delay is not None else None

    @property
    def duration(self):
        if not self.started_at or not self.finished_at:
            return None
        return self.finished_at - self.started_at

    @property
    def duration_seconds(self):
        duration = self.duration
        return round(duration.total_seconds()) if duration is not None else None

    async def fetch_jobs(self, params: Dict[str, Any] = None):
        request = GET(f'/projects/{self.project_id}/pipelines/{self.id}/jobs', params)
        async for info in self.api.collect_all_pages_(request):
            info['project_id'] = self.project_id
            yield Job(self.api, info)

    async def refetch_info(self) -> None:
        project_id = self._info['project_id']
        self._info = await self._api.call(GET(f'/projects/{self.project_id}/pipelines/{self.id}'))
        self._info['project_id'] = project_id


@dataclass(frozen=True)
class Stage:
    name: str
    jobs: List[Job] = field(default_factory=list)

    @property
    def created_at(self):
        # pylint: disable=not-an-iterable
        values = [x.created_at for x in self.jobs if x.created_at]
        return min(values) if values else None

    @property
    def started_at(self):
        # pylint: disable=not-an-iterable
        values = [x.started_at for x in self.jobs if x.started_at]
        return min(values) if values else None

    @property
    def finished_at(self):
        # pylint: disable=not-an-iterable
        values = [x.finished_at for x in self.jobs if x.finished_at]
        return max(values) if values else None

    @property
    def delay(self):
        if not self.started_at:
            return None
        return self.started_at - self.created_at

    @property
    def delay_seconds(self):
        delay = self.delay
        return round(delay.total_seconds()) if delay is not None else None

    @property
    def duration(self):
        if not self.started_at or not self.finished_at:
            return None
        return self.finished_at - self.started_at

    @property
    def duration_seconds(self):
        duration = self.duration
        return round(duration.total_seconds()) if duration is not None else None
