# Copyright 2019-2020  Ternaris.
# SPDX-License-Identifier: Apache-2.0

# pylint: disable=too-many-instance-attributes,too-few-public-methods

from inspect import cleandoc
from typing import Dict, List, Optional, Union

from pydantic import (
    BaseModel as _BaseModel,
    Field,
)
from ruamel.yaml import YAML


# TODO: Validation: Label names must not contain commas


def load(path):
    yaml = YAML(typ='safe')
    return Config.parse_obj(yaml.load(path))


class FString(str):
    """An expression to be used in a python f-string.

    The evaluation context of the f-string depends on where in the
    configuration it is used. See :class:`.Rule` for an example.
    """

    @classmethod
    def __get_validators__(cls):
        yield cls.validate

    @classmethod
    def validate(cls, value):
        return cls(value)

    def eval(self, **kw):
        if '{' not in self:
            return self
        return eval(f"f'{self}'", None, kw)  # pylint: disable=eval-used


class FilterString(str):
    """A string defining a filter to apply on issues and merge requests.

    TODO: Document all filters here or point to documentation.
    """

    @classmethod
    def __get_validators__(cls):
        yield cls.validate

    @classmethod
    def validate(cls, value):
        return cls(value)


def clean(docstring):
    return cleandoc(docstring.strip())


class DocGen:
    def __init__(self, target):
        self.target = target
        self.doc = target.__doc__

    def __get__(self, *args):
        fields = self.target.__fields__
        if not fields:
            return self.doc

        preamble = cleandoc(self.doc).rstrip()
        return f'{preamble}\n\n{self._google_style(fields)}'

    def _google_style(self, fields):
        modprefix = f'{self.target.__module__}.'
        lines = ['Args:']
        for name, field in fields.items():
            type_ = field.outer_type_
            if hasattr(type_, '__name__'):
                typestr = type_.__name__
            else:
                typestr = str(type_).replace('typing.', '')\
                                    .replace(modprefix, '')
            parts = (field.field_info.description or '').splitlines()
            first = parts.pop(0) if parts else ''
            flags = f'(Default: {field.default}) ' if field.default else ''
            lines.append(f'  {name} ({typestr}): {flags}{first}')
            lines.extend(f'    {x}' for x in parts if x)
        return '\n'.join(lines)


class BaseModel(_BaseModel):
    class Config:
        allow_mutation = False

    def asdict(self):  # TODO: BBB
        return dict(self)

    @classmethod
    def from_dict(cls, dct):  # TODO: BBB
        return cls(**dct)

    def __init_subclass__(cls) -> None:
        cls.__doc__ = DocGen(cls)  # type: ignore


class GitlabGroup(BaseModel):
    """A GitLab group."""

    gitlab_url: str = Field(..., description='URL to the GitLab instance.')
    api_token: str = Field(..., description='API token to access group with.')
    group_path: str = Field(..., description='Full path to group.')


class GitlabProject(BaseModel):
    """A GitLab project."""

    gitlab_url: str = Field(..., description='URL to the GitLab instance.')
    api_token: str = Field(..., description='API token to access project with.')
    project_path: str = Field(..., description='Full path to project.')


class Action(BaseModel):
    """An Action to be applied to an issue or merge request."""


class AddLabelsAction(Action):
    """Add labels to issue or merge request."""

    add_labels: List[FString]


class RemoveLabelsAction(Action):
    remove_labels: List[FString]


class AssignAction(Action):
    assign: List[FString]


class ReassignAction(Action):
    reassign: List[FString]


class Comment(BaseModel):
    mention: List[FString]


class CommentAction(Action):
    comment: Comment


ActionUnion = Union[
    AddLabelsAction,
    RemoveLabelsAction,
    AssignAction,
    ReassignAction,
    CommentAction,
]


class LabelSetCheck(BaseModel):
    """Check item for a set of labels."""

    label_set: List[str] = Field(..., description='List of mutually exclusive labels.')
    required: bool = Field(True, description='Indicate whether a label of this set is required.')


CheckUnion = Union[
    LabelSetCheck,
]


class Rule(BaseModel):
    """Rules to be applied on issues or merge requests."""

    actions: List[ActionUnion]
    filter: Optional[FilterString]
    checks: Optional[List[CheckUnion]]


class Checker(BaseModel):
    rules: List[Rule]
    """List of :class:`.Rule` to check issues and/or merge requests for."""


class CheckGroupIssues(Checker, GitlabGroup):
    pass


class CheckGroupMergeRequests(Checker, GitlabGroup):
    pass


class CheckProjectIssues(Checker, GitlabProject):
    pass


class CheckProjectMergeRequests(Checker, GitlabProject):
    pass


CheckerUnion = Union[
    CheckGroupIssues,
    CheckGroupMergeRequests,
    CheckProjectIssues,
    CheckProjectMergeRequests,
]


class ProjectSyncSource(GitlabProject):
    """A project to sync issues from."""

    static_labels: List[str] = Field((), description=clean("""
    List of labels to be always added to target issues linking to this source.
    """))

    label_map: Dict[str, str] = Field(default_factory=dict, description=clean("""
    A dictionary mapping target label names (keys) to source label names (values).

    The source is authoritative for these labels. Changes to these
    labels on the target will be gone after the next sync.
    """))


class ProjectSyncTarget(GitlabProject):
    """A project to sync issues to."""

    managed_label: str = Field('managed', description=clean("""
    Label indicating that a target issue is managed by GRGE.
    """))


class SyncExternalIssues(BaseModel):
    """Instructions to sync issues from source projects to a target project."""

    sources: List[ProjectSyncSource] = Field(..., description=clean("""
    One or more :class:`.ProjectSyncSource` to fetch issues from.
    """))

    target: ProjectSyncTarget = Field(..., description=clean("""
    A :class:`.ProjectSyncTarget` to create issues in.
    """))


class SyncStoriesAndTasks(BaseModel):
    gitlab_url: str = Field(..., description='URL to the GitLab instance.')
    api_token: str = Field(..., description='API token to access project with.')
    project_path: str = Field(..., description='Path to project where stories are looked for.')

    task_project_path: Optional[str] = Field(None, description=clean("""
    Optionally, place tasks into different project. Normally they live next to the story issues.
    """))

    active_story_label: str = Field('active', description=clean("""
    Optionally, specify label that indicates an active story (default: 'active').
    """))

    active_task_label: str = Field('active', description=clean("""
    Optionally, specify label that indicates an active task (default: 'active').
    """))

    story_labels: List[str] = Field(['story'], description=clean("""
    Labels that define a story, used to search for stories (default: ['story']).
    """))

    task_labels: List[str] = Field(['task'], description=clean("""
    Labels that define a task, used to search for and create tasks (default: ['task']).
    """))


class Job(BaseModel):
    name: Optional[str]
    sleep_interval: int


class CheckGroupIssuesJob(Job):
    check_group_issues: CheckGroupIssues


class CheckGroupMergeRequestsJob(Job):
    check_group_merge_requests: CheckGroupMergeRequests


class CheckProjectIssuesJob(Job):
    check_project_issues: CheckProjectIssues


class CheckProjectMergeRequestsJob(Job):
    check_project_merge_requests: CheckProjectMergeRequests


class SyncExternalIssuesJob(Job):
    sync_external_issues: SyncExternalIssues


class SyncStoriesAndTasksJob(Job):
    sync_stories_and_tasks: SyncStoriesAndTasks


JobUnion = Union[
    CheckGroupIssuesJob,
    CheckGroupMergeRequestsJob,
    CheckProjectIssuesJob,
    CheckProjectMergeRequestsJob,
    SyncExternalIssuesJob,
    SyncStoriesAndTasksJob,
]


class Config(BaseModel):
    """GRGE's configuration is a yaml file with a list of jobs.

    Example:
      .. code-block:: yaml

         jobs:
           - sleep_interval: 30
             sync_stories_and_tasks:
               api_token: TOKEN
               gitlab_url: https://gitlab.com
               project_path: ApexAI/grge-test/combined

    """

    jobs: List[JobUnion] = Field(..., description='List of jobs')
