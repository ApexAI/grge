# Copyright 2018 - 2019  Ternaris.
# SPDX-License-Identifier: Apache-2.0

import logging as log
from typing import Dict, Union
from urllib.parse import quote_plus

from pkg_resources import parse_version

from . import gitlab


DELETE, GET, POST = gitlab.DELETE, gitlab.GET, gitlab.POST


class Release(gitlab.Resource):
    project_id = None

    @classmethod
    async def create(cls, api: gitlab.Api, id_or_path: Union[int, str], params: Dict) -> 'Release':
        log.info('Create %s: %s, %r', cls.__name__, id_or_path, params)
        id_or_path = quote_plus(id_or_path) if isinstance(id_or_path, str) else id_or_path
        info = await api.call(POST(f'/projects/{id_or_path}/releases', params))
        release = cls(api, info)
        release.project_id = id_or_path
        return release

    @classmethod
    async def fetch_all(cls, api, id_or_path, params=None):
        id_or_path = quote_plus(id_or_path) if isinstance(id_or_path, str) else id_or_path
        infos = await api.collect_all_pages(GET(f'/projects/{id_or_path}/releases', params))
        releases = [cls(api, info) for info in infos]
        for release in releases:
            release.project_id = id_or_path
        return releases

    @classmethod
    async def fetch_by_tag_name(cls, api, id_or_path, tag_name):
        id_or_path = quote_plus(id_or_path) if isinstance(id_or_path, str) else id_or_path
        info = await api.call(GET(f'/projects/{id_or_path}/releases/{tag_name}'))
        release = cls(api, info)
        release.project_id = id_or_path
        return release

    async def delete(self):
        await self.api.call(DELETE(f'/projects/{self.project_id}/releases/{self.tag_name}'))

    @property
    def assets(self):
        return self.info['assets']

    @property
    def description(self):
        return self.info['description'] or ''

    @property
    def description_html(self):
        return self.info['description_html'] or ''

    @property
    def name(self):
        return self.info['name']

    @property
    def tag_name(self):
        return self.info['tag_name']

    @property
    def version(self):
        return parse_version(self.tag_name)
