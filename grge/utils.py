# Copyright 2017 - 2019  Ternaris.
# SPDX-License-Identifier: Apache-2.0

"""Utility functions."""

import asyncio
import json
import os
import sys
from contextlib import contextmanager
from functools import update_wrapper
from pathlib import Path
from typing import Callable, List

import click
import git
import yarl


def asyncio_run(func):
    """Wrap async function to run synchronously.

    This is useful to implement click commands using async functions.
    """
    def sync_func(*args, **kw):
        try:
            return asyncio.run(func(*args, **kw))
        except asyncio.CancelledError:
            err('Stopped.', exit=1)
    return update_wrapper(sync_func, func)


def echo(*args, **kw):
    """Wrap print to let linter forbid print usage."""
    print(*args, **kw)  # noqa: T001


def err(*args, exit=None, **kw):
    """Print to stderr and optionally exit."""
    print(*args, **kw, flush=True, file=sys.stderr)  # noqa: T001
    if exit is not None:
        sys.exit(exit)


def find_file(filename, cwd=None, parent=False, file_okay=True, dir_okay=True):
    """Find file starting from cwd upwards.

    Return parent instead of file if parent is set to True.
    """
    assert file_okay or dir_okay
    path = Path(cwd or '.').absolute()
    while True:
        envfile = path / filename
        if file_okay and envfile.is_file() or dir_okay and envfile.is_dir():
            return envfile.parent if parent else envfile
        if path == path.parent:
            return None
        path = path.parent


@contextmanager
def launch_pdb_on_exception(launch=True):
    """Return contextmanager launching pdb upon exception.

    Use like this, to toggle via env variable:

    with launch_pdb_on_exception(os.environ.get('PDB')):
        cli()
    """
    # pylint: disable=broad-except
    try:
        yield
    except Exception:  # noqa
        if launch:
            import pdb  # pylint: disable=import-outside-toplevel
            pdb.xpm()  # pylint: disable=no-member
        else:
            raise


def get_project_url(cwd=None):
    # We don't want a worktree, but the main working directory
    gitdir = find_file('.git', cwd=cwd, file_okay=False, parent=True)
    if not gitdir:
        ctx = click.get_current_context()
        ctx.fail('Run from within git repository or provide PROJECT_URL!')

    repo = git.Repo(gitdir)
    remote = next((x for x in repo.remotes if x.name == 'origin'), repo.remotes[0])
    project_url = next(remote.urls).rsplit('.git', 1)[0]
    if project_url.startswith('git@'):
        project_url = f'https://{project_url.split("@", 1)[1].replace(":", "/", 1)}'
    project_url = yarl.URL(project_url)
    if project_url.user or project_url.password:
        project_url = project_url.with_user(None)
    return project_url


async def get_token(project_url, envvar=None):
    token = os.environ.get(envvar) if envvar else None
    if token:
        return token

    credsfile, *replace = os.environ.get('GRGE_CREDS_FILE', '').split(':')
    if credsfile:
        host = project_url.host.replace(*replace) if replace else project_url.host
        try:
            return json.loads(Path(credsfile).read_text())[host]['token']
        except KeyError:
            pass

    try:
        token = (await pass_get(f'{project_url.host}')).decode('utf-8')
    except FileNotFoundError:
        echo('ERROR: Could not find token to authenticate with GitLab API.')
        echo('Install https://www.passwordstore.org/ or in CI set GRGE_RELEASE_TOKEN')
        sys.exit(1)
    return token


async def pass_get(key):
    proc = await asyncio.create_subprocess_exec('pass', key, stdout=asyncio.subprocess.PIPE)
    password = (await proc.stdout.read()).rstrip()
    await proc.wait()
    return password


def popwhile(pred: Callable, lst: List) -> List:
    """Pop items from list while pred(lst[-1]) is True."""
    arg = None
    args: List = []
    while lst and pred(lst[-1]):
        arg = lst.pop()
        args.insert(0, arg)
    return args


def read_changelog_section(changes, tag_name):
    started = False
    lines = []
    for line in changes.splitlines():
        line = line.strip()
        if not started and line == f'.. _{tag_name}:':
            started = True
        elif started and line.startswith('.. _v'):
            started = False
        elif started:
            lines.append(line)
    return '\n'.join(lines).strip() if lines else None


async def rst_to_markdown(rst):
    proc = await asyncio.create_subprocess_exec('pandoc', '-f', 'rst', '-t', 'markdown', '-',
                                                stdin=asyncio.subprocess.PIPE,
                                                stdout=asyncio.subprocess.PIPE)
    proc.stdin.write(rst.encode('utf-8'))
    proc.stdin.write_eof()
    await proc.stdin.drain()
    markdown = (await proc.stdout.read()).decode('utf-8')
    await proc.wait()
    return markdown
