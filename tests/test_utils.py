# Copyright 2019  Ternaris.
# SPDX-License-Identifier: Apache-2.0

import asyncio

import git
import pytest
import yarl

from grge import utils


def test_asyncio_run():
    async def async_func(*args, cancel=False, **kw):
        """DOC."""
        if cancel:
            raise asyncio.CancelledError()
        return (args, kw)
    sync_func = utils.asyncio_run(async_func)
    assert sync_func.__doc__ == async_func.__doc__

    args, kw = sync_func(1, 2, 3, a='A', b='B')
    assert args == (1, 2, 3)
    assert kw == {'a': 'A', 'b': 'B'}

    with pytest.raises(SystemExit) as einfo:
        sync_func(cancel=True)
    assert einfo.value.code == 1


def test_find_file(tmp_path):
    path = utils.find_file('pyproject.toml')
    assert 'build-system' in path.read_text()

    foo_dir = (tmp_path / 'foo')
    foo_dir.mkdir()
    subdir = tmp_path / 'subdir'
    subdir.mkdir()
    subsubdir = subdir / 'subdir'
    subsubdir.mkdir()
    foo_file = subsubdir / 'foo'
    foo_file.touch()

    assert utils.find_file('foo', cwd=tmp_path) == foo_dir
    assert utils.find_file('foo', cwd=subdir) == foo_dir
    assert utils.find_file('foo', cwd=subdir, dir_okay=False) is None
    assert utils.find_file('foo', cwd=subsubdir) == foo_file
    assert utils.find_file('foo', cwd=subsubdir, file_okay=False) == foo_dir


def test_launch_pdb_can_be_disabled():
    @utils.launch_pdb_on_exception(False)
    def value_error():
        raise ValueError()

    with pytest.raises(ValueError):
        value_error()


def test_get_project_url(tmp_path):
    worktree = tmp_path / 'WT'
    worktree.mkdir()
    (worktree / '.git').touch()
    repo = git.Repo.init(tmp_path)
    first = repo.create_remote('first', 'https://user@gitlab.com/foo/bar')
    second = repo.create_remote('second', 'https://user:password@gitlab.com/foo/bar')
    third = repo.create_remote('third', 'THIRD')
    origin = repo.create_remote('origin', 'git@gitlab.com:foo/bar')
    assert repo.remotes == [first, second, third, origin]

    project_url = utils.get_project_url(worktree)
    assert project_url == yarl.URL('https://gitlab.com/foo/bar')

    repo.delete_remote('origin')
    project_url = utils.get_project_url(worktree)
    assert project_url == yarl.URL('https://gitlab.com/foo/bar')

    repo.delete_remote('first')
    project_url = utils.get_project_url(worktree)
    assert project_url == yarl.URL('https://gitlab.com/foo/bar')

    repo.delete_remote('second')
    project_url = utils.get_project_url(worktree)
    assert project_url == yarl.URL('THIRD')
